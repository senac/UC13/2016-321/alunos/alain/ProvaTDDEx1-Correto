
package br.com.apgf.ex1;

public class Pessoa {

    private String pessoa1;
    private String pessoa2;
    private String pessoa3;
    private int idade1;
    private int idade2;
    private int idade3;

    public Pessoa() {
    }

    public Pessoa(String pessoa1, int idade1, String pessoa2,  int idade2, String pessoa3, int idade3) {
        this.pessoa1 = pessoa1;
        this.idade1 = idade1;
        this.pessoa2 = pessoa2;
        this.idade2 = idade2;
        this.pessoa3 = pessoa3;
        this.idade3 = idade3;
    }

   

    public String getPessoa1() {
        return pessoa1;
    }

    public void setPessoa1(String pessoa1) {
        this.pessoa1 = pessoa1;
    }

    public String getPessoa2() {
        return pessoa2;
    }

    public void setPessoa2(String pessoa2) {
        this.pessoa2 = pessoa2;
    }

    public String getPessoa3() {
        return pessoa3;
    }

    public void setPessoa3(String pessoa3) {
        this.pessoa3 = pessoa3;
    }

    public int getIdade1() {
        return idade1;
    }

    public void setIdade1(int idade1) {
        this.idade1 = idade1;
    }

    public int getIdade2() {
        return idade2;
    }

    public void setIdade2(int idade2) {
        this.idade2 = idade2;
    }

    public int getIdade3() {
        return idade3;
    }

    public void setIdade3(int idade3) {
        this.idade3 = idade3;
    }
    
}

   