
package br.com.apgf.ex1Test;

import br.com.apgf.ex1.CalculoIdade;
import br.com.apgf.ex1.Pessoa;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculoIdadeTest {
    
    @Test
    public void deveMostrarPessoaDeMaiorIdadeEPessoaDeMenorIdade(){
    Pessoa p = new Pessoa("Mateus" ,10 ,"Marcos" ,12 ,"Lucas" ,15);
    CalculoIdade calculo = new CalculoIdade();
    String PessoaMaisVelha = calculo.idadeMaior(p);
    assertEquals("PessoaMaisVelha",p.getPessoa3(), PessoaMaisVelha);
    String PessoaMaisNova = calculo.idadeMenor(p);
    assertEquals("PessoaMaisNova",p.getPessoa1(), PessoaMaisNova);
    }    
}
